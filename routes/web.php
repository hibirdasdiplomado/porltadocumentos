<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $carreras = App\Carrera::all();
    return view('home', compact('carreras'));
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
